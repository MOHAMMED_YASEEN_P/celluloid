from __future__ import unicode_literals
from djongo import models
from django.conf import settings
import datetime
from django.db import models as md
from django.db.models.signals import post_save


class City(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.name


class Movie(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=100, null=True)
    language = models.CharField(max_length=50)
    duration = models.DurationField(default=datetime.timedelta(hours=2))

    def __str__(self):
        return self.name

class Genre(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=200)
    movie = models.ManyToManyField(Movie,related_name = 'tags')
    
    def __str__(self):
        return self.name

class Theater(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=200, null=True)
    city = models.CharField(max_length=200, null=True)
    location = models.CharField(max_length=200, null=True)
    row = models.IntegerField(default=0)
    column = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Show(models.Model):
    _id = models.ObjectIdField()
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    theater = models.ForeignKey(Theater, on_delete=models.CASCADE)
    date_and_time = models.DateTimeField(datetime.time(12,0,0))
    show_name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return f"{self.theater}-{self.show_name}-{self.movie}"


class Seat(models.Model):
    _id = models.ObjectIdField()
    theater = models.ForeignKey(Theater, on_delete=models.CASCADE)
    row = models.IntegerField(default=0)
    column = models.IntegerField(default=0)
    def __str__(self):
        name = f"({self.row},{self.column})"
        return name


class Booking(models.Model):
    _id = models.ObjectIdField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    seat = models.ForeignKey(Seat, on_delete=models.CASCADE)
    show = models.ForeignKey(Show, on_delete=models.CASCADE)
    booking_reference_number = models.CharField(max_length=100, null=True)
    status = models.CharField(max_length=50, null=True)

    def __str__(self):
        name = f"{self.show.show_name}-{self.show.movie.name}({self.show.date_and_time})"
        return name

class Snack(models.Model):
    _id = models.ObjectIdField()
    name = models.CharField(max_length=100, null=True)
    available_stock = models.IntegerField(default=0)
    price = models.IntegerField(default=0)
 
    def __str__(self):
        return self.name

class SnackBooking(models.Model):
    _id = models.ObjectIdField()
    booking =  models.ForeignKey(Booking, on_delete=models.CASCADE)
    snack = models.ForeignKey(Snack, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    status = models.CharField(max_length=50, null=True)

    def __str__(self):
        return f"{self.snack.name}-{self.booking.booking_reference_number}"


class BookmarkedShow(models.Model):
    _id = models.ObjectIdField(primary_key=True,unique=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    show = models.ForeignKey(Show, on_delete=models.CASCADE)

    def __str__(self):
        return self.show
