from django.core.management.base import BaseCommand, CommandError
from movies.models import Movie
from datetime import timedelta


movie_list = [
        {
        'language': "Malayalam",
        'name': 'Anjaam Pathira',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "Malayalam",
        'name': 'Lucifer',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "Malayalam",
        'name': 'Kumbalangi Nights',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "Malayalam",
        'name': 'Shylock',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "English",
        'name': 'The Invisible Man',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "English",
        'name': 'Bloodshot',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "Hindi",
        'name': 'Thappad',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "Hindi",
        'name': 'War',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "Tamil",
        'name': 'Pattas',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "Tamil",
        'name': 'Darbar',
        'duration': timedelta(hours=2,minutes=30),
        },
        {
        'language': "Tamil",
        'name': 'kaithi',
        'duration': timedelta(hours=2,minutes=30),
        },
    ]

class Command(BaseCommand):
    def add_movie(self):
        for movie in movie_list: 
            Movie.objects.create(
                name=movie['name'],
                language=movie['language'],
                duration=movie['duration'],
                )

    def handle(self, *args, **options):
        self.add_movie()