from django.core.management.base import BaseCommand, CommandError
from movies.models import City

cities = ['Mumbai', 'Delhi','Bangalore', 'Hydrabad', 'Chennai', 'Kolkata']
class Command(BaseCommand):
    def add_city(self):
        for city in cities: 
            City.objects.create(
                name = city,
                )

    def handle(self, *args, **options):
        self.add_city()