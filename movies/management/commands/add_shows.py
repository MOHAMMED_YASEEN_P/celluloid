from django.core.management.base import BaseCommand, CommandError
from movies.models import Show, Movie, Theater
from random import choice
import datetime


movies = list(Movie.objects.all())
theaters = list(Theater.objects.all())
today = datetime.date.today()
tomorrow = today + datetime.timedelta(days = 1) 
day_after_tomorrow = today + datetime.timedelta(days = 2) 

class Command(BaseCommand):
    def add_show(self):
        for theater in theaters: 
            Show.objects.create(movie=choice(movies), theater=theater, date_and_time=datetime.datetime.combine(today,datetime.time(10,0,1)),show_name='Morning Show')
            Show.objects.create(movie=choice(movies), theater=theater, date_and_time=datetime.datetime.combine(today,datetime.time(14,0,1)),show_name='First Show')
            Show.objects.create(movie=choice(movies), theater=theater, date_and_time=datetime.datetime.combine(today,datetime.time(21,0,1)),show_name='Second Show')

            Show.objects.create(movie=choice(movies), theater=theater, date_and_time=datetime.datetime.combine(tomorrow,datetime.time(10,0,1)),show_name='Morning Show')
            Show.objects.create(movie=choice(movies), theater=theater, date_and_time=datetime.datetime.combine(tomorrow,datetime.time(14,0,1)),show_name='First Show')
            Show.objects.create(movie=choice(movies), theater=theater, date_and_time=datetime.datetime.combine(tomorrow,datetime.time(21,0,1)),show_name='Second Show')

            Show.objects.create(movie=choice(movies), theater=theater, date_and_time=datetime.datetime.combine(day_after_tomorrow,datetime.time(10,0,1)),show_name='Morning Show')
            Show.objects.create(movie=choice(movies), theater=theater, date_and_time=datetime.datetime.combine(day_after_tomorrow,datetime.time(14,0,1)),show_name='First Show')
            Show.objects.create(movie=choice(movies), theater=theater, date_and_time=datetime.datetime.combine(day_after_tomorrow,datetime.time(21,0,1)),show_name='Second Show')
    def handle(self, *args, **options):
        self.add_show()