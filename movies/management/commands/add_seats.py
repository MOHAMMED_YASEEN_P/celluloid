from movies.models import Seat, Theater
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    def add_seats(self):
        theaters = list(Theater.objects.all())
        for theater in theaters:
            for row in range(theater.row):
                for column in range(theater.column):
                    Seat.objects.create(row=row, column=column, theater=theater)
    def handle(self, *args, **options):
        self.add_seats()