from django.core.management.base import BaseCommand, CommandError
from movies.models import Genre, Movie
from random import sample

genres = ['Action' ,'Animation' ,'Comedy' ,'Crime' ,'Drama' ,'Experimental' ,'Fantasy' ,'Historical' ,'Horror' ,'Romance' ,'Science Fiction' ,'Thriller' ,'Western' ,'Other',]
class Command(BaseCommand):
    def add_genre(self):
        movie_list = list(Movie.objects.all())
        for genre in genres: 
            movies = sample(movie_list,3)
            genre_obj= Genre(name = genre)
            genre_obj.save()
            for movie_obj in movies:
                genre_obj.movie.add(movie_obj._id)


    def handle(self, *args, **options):
        self.add_genre()
