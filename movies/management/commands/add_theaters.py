from django.core.management.base import BaseCommand, CommandError
from movies.models import Theater


theater_list = [
        {
        'theater': "PVR Cinemas (The Forum Mall)",
        'city': 'Bangalore',
        'location': 'Koramangala',
        'row':10,
        'column':10,
        },
        {
        'theater': "PVR Cinemas (Orion Mall)",
        'city': 'Bangalore',
        'location': 'Malleswaram',
        'row':11,
        'column':10,
        },
        {
        'theater': "INOX Leisure Ltd",
        'city': 'Bangalore',
        'location': 'marathahalli',
        'row':10,
        'column':10,
        },
        {
        'theater': "Cinepolis Multiplex",
        'city': 'Bangalore',
        'location': 'S.G playa',
        'row':10,
        'column':10,
        },
        {
        'theater': "Rockline Cinemas",
        'city': 'Bangalore',
        'location': 'jahalahalli',
        'row':11,
        'column':8,
        },
        {
        'theater': "Delite Cinema",
        'city': 'Delhi',
        'location': 'New Delhi',
        'row':10,
        'column':8,
        },
        {
        'theater': "PVR Naraina",
        'city': 'Delhi',
        'location': 'New Delhi',
        'row':10,
        'column':8,
        },
        {
        'theater': "PVR Plaza-CP",
        'city': 'Delhi',
        'location': 'New Delhi',
        'row':15,
        'column':11,
        },
    ]

class Command(BaseCommand):
    def add_theater(self):
        for theater in theater_list: 
            Theater.objects.create(
                name=theater['theater'],
                city=theater['city'],
                location=theater['location'],
                row=theater['row'],
                column=theater['column'],
                )

    def handle(self, *args, **options):
        self.add_theater()