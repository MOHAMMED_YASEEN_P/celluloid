from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.movie_list, name='movie_list'),
    path('city/<str:city>/<str:cdate>', views.city_movie_list, name='city_movie_list'),
    path('city/<str:city>/<str:cdate>/<str:movie>',views.theater_list, name="theater_list"),
    path('city/<str:city>/<str:cdate>/<str:movie>/<str:theater>/', views.select_seat, name="select_seat"),
    path('confirm-seat/<str:show_id>', views.confirm_seats, name="confirms_seat"),
    path('profile/', views.profile, name='profile'),
    path('cancel-ticket/<str:brn>', views.cancel_ticket, name='profile'),
    path('ticket/<str:brn>', views.generated_ticket, name='ticket'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

