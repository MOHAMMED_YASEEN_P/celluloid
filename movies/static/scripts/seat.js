const container = document.querySelector('.container');
const seats = document.querySelectorAll('.row .seat:not(.occupied)');
const count = document.getElementById('count');
// const price = document.getElementById('price');
const selection = document.getElementById('seats-selection');

// let ticketPrice = +movieSelect.value;



const updateSelectedSeats = () => {
  const selectedSeats = document.querySelectorAll('.row .selected');
  // const seatsIndex = [...selectedSeats].map(seat => [...seats].indexOf(seat));
  // localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex));
  const selectedSeatsCount = selectedSeats.length;
  count.innerText = selectedSeatsCount;
  selection.innerText = [...seatsSelectedByCurrentUser]
  price.innerText = selectedSeatsCount * ticketPrice;
};


// Seat select event
var seatsSelectedByCurrentUser = new Set()
container.addEventListener('click', e => {
  if (
    e.target.classList.contains('seat') &&
    !e.target.classList.contains('occupied') 
  ) {
    // alert(e.target.id)
    e.target.classList.toggle('selected');
    if(e.target.classList.contains('selected')){
      seatsSelectedByCurrentUser.add(e.target.id)
    }
    else{
      seatsSelectedByCurrentUser.delete(e.target.id)
    }
    document.getElementById("selected-seats").value= [...seatsSelectedByCurrentUser]
    console.log(...seatsSelectedByCurrentUser)
    updateSelectedSeats();
  }
});
// Movie select event
movieSelect.addEventListener('change', e => {
  ticketPrice = +e.target.value;
  selectedMovie(e.target.selectedIndex, e.target.value);

  updateSelectedSeatsCount();
});
