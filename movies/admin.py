from django.contrib import admin
from .models import Movie, Show, Booking, Seat, Theater,\
    SnackBooking, Snack, City, BookmarkedShow, Genre

# Register your models here.
admin.site.register(City)
admin.site.register(Movie)
admin.site.register(Genre)
admin.site.register(Show)
admin.site.register(Theater)
admin.site.register(Seat)
admin.site.register(Booking)
admin.site.register(Snack)
admin.site.register(SnackBooking)
admin.site.register(BookmarkedShow)
