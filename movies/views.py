from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Show, Seat, Theater, Booking
from datetime import date, timedelta, datetime
from django.urls import reverse
import pytz
import qrcode
from os import path, remove

# Create your views here.
def movie_list(request):
    return render(request, 'movies/movie_list.html', {'start_date': str(date.today()), 'end_date':str(date.today()+timedelta(days=5))})


def city_movie_list(request, city, cdate):
    # import pdb; pdb.set_trace()
    year,month,day = [int(_) for _ in cdate.split("-")]
    cdate = date(year, month, day)
    shows = Show.objects.select_related('movie').filter(theater__city=city,date_and_time__gt=cdate,date_and_time__lt=cdate+timedelta(days=1))
    movies = []
    unique_shows =[]
    for show in shows:
        if show.movie.name not in movies:
            movies.append(show.movie.name)
            unique_shows.append(show)
    return render(request, 'movies/city_movie_list.html', {'shows': unique_shows, 'date':f"{year}-{month}-{day}"})

    # 

def theater_list(request,city,cdate,movie):
    year,month,day = [int(_) for _ in cdate.split("-")]
    cdate = date(year, month, day)
    shows = Show.objects.select_related('movie').filter(theater__city=city,date_and_time__gt=cdate,date_and_time__lt=cdate+timedelta(days=1),movie__name=movie)
    for show in shows:
        show.date_and_time=str(show.date_and_time)
    return render(request, 'movies/theater_list.html', {'shows': shows,})

def select_seat(request,city,cdate,movie,theater):
    theater = Theater.objects.get(name=theater)
    show = Show.objects.get(date_and_time=cdate,theater__name=theater)
    bookings = Booking.objects.filter(show=show)
    bookings = [(booking.seat.row,booking.seat.column) for booking in bookings]
    row = show.theater.row
    column = show.theater.column
    seats = []
    for row_num in range(row):
        temp = []
        for column_num in range(column):
            if (row_num,column_num) in bookings:
                if column_num == 0: 
                    temp.append(
                        {   
                            'row_name': chr(row_num+65),
                            'status': "reserved",
                            'id': f"{chr(row_num+65)}{column_num+1}"
                        }
                        )
                else:
                    temp.append(
                        {   
                            'status': "reserved",
                            'id': f"{chr(row_num+65)}{column_num+1}"
                        }
                    )
            else:
                if column_num == 0:
                    temp.append(
                        {
                            'row_name': chr(row_num+65),
                            'status':"free",
                            'id':f"{chr(row_num+65)}{column_num+1}"
                        }
                        )
                else:
                    temp.append(
                        {
                            'status':"free",
                            'id':f"{chr(row_num+65)}{column_num+1}"
                        }
                        )
        seats.append(temp)
    return render(request, 'movies/seat_list.html', {'row':range(theater.row), 'column':range(theater.column),'booking':seats,"show":show._id})

def confirm_seats(request,show_id):
    show = Show.objects.get(_id=show_id)
    theater = Theater.objects.get(_id=show.theater._id)
    seats = request.POST['selected-seats']
    seats = [(ord(seat[0])-65,int(seat[1:])-1) for seat in seats.split(",")]
    BRN = hash(show_id+str(datetime.now()))
    for seat in seats:
        print(seat[0],seat[1], type(seat[0]))
        seat_object = Seat.objects.get(row=seat[0],column=seat[1],theater=theater)
        Booking.objects.create(user=request.user, seat=seat_object, show=show, booking_reference_number=BRN)
    return redirect('ticket',brn=str(BRN))


def make_ticket_qr_code(tickets):
    for ticket in tickets:
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=2,
            border=0,
        )
        qr.add_data(
            {
            'Ticket-status': tickets[ticket]['status'],
            'BRN': ticket,
            'Theater': tickets[ticket]['theater'],
            'Show': tickets[ticket]['show'],
            'Show_date': tickets[ticket]['show_date'],
            'Seats': tickets[ticket]['seats'],
            'Name': tickets[ticket]['name'],
            } 
            )
        qr.make(fit=True)

        img = qr.make_image(fill_color="black", back_color="white")
        img.save(f"movies/static/images/{ticket}.png")


def profile(request):
    boking_history = Booking.objects.filter(user=request.user)
    booking_deatils = {}
    utc=pytz.UTC
    for booking in boking_history:
        if booking.booking_reference_number not in booking_deatils:
            booking_deatils[booking.booking_reference_number]={
                'movie': booking.show.movie.name,
                'theater': booking.show.theater.name,
                'show': f"{booking.show.show_name} - {str(booking.show.date_and_time).split()[1]}",
                'show_date': str(booking.show.date_and_time).split(" ")[0],
                'no_of_seats': 1,
                'seats': f"{chr(booking.seat.row+65)}{booking.seat.column+1}",
                'name': request.user.username
            }
            booking_date = booking.show.date_and_time.replace(tzinfo=utc)
            date_now = datetime.now()
            date_now = date_now.replace(tzinfo=utc)
            if booking_date < date_now:
                booking_deatils[booking.booking_reference_number]["status"] = "Expired"
            else:
                booking_deatils[booking.booking_reference_number]["status"] = "Active"
        else:
            booking_deatils[booking.booking_reference_number]["no_of_seats"] += 1
            booking_deatils[booking.booking_reference_number]["seats"] += f", {chr(booking.seat.row+65)}{booking.seat.column+1}"

    tickets = {}
    for ticket in booking_deatils:
        if not path.exists(f"{ticket}.png"):
            tickets[ticket] = booking_deatils[ticket]
    make_ticket_qr_code(tickets)
    return render(request, 'movies/profile.html',{'boking_history': booking_deatils})

def cancel_ticket(request, brn):
    ticket = Booking.objects.filter(booking_reference_number=brn)
    if request.user == ticket[0].user:
        for seat in ticket:
            seat.delete()
            remove(f"movies/static/images/{brn}.png")
    return redirect(reverse('profile'))

def generated_ticket(request, brn):
    booking = Booking.objects.filter(booking_reference_number=brn)
    booking_deatils = {brn:{}}
    booking_deatils[brn]['movie'] = booking[0].show.movie.name
    booking_deatils[brn]['theater'] = booking[0].show.theater.name
    booking_deatils[brn]['show'] = f"{booking[0].show.show_name} - {str(booking[0].show.date_and_time).split()[1]}"
    booking_deatils[brn]['show_date'] = str(booking[0].show.date_and_time).split(" ")[0]
    booking_deatils[brn]['no_of_seats'] = len(booking)
    for seat_obj in booking: 
        if 'seats' in booking_deatils[brn]:
            booking_deatils[brn]['seats'] += f", {chr(seat_obj.seat.row+65)}{seat_obj.seat.column+1}"
        else:
            booking_deatils[brn]['seats'] = f"{chr(seat_obj.seat.row+65)}{seat_obj.seat.column+1}"
    booking_deatils[brn]['name'] = request.user.username
    utc=pytz.UTC
    booking_date = booking[0].show.date_and_time.replace(tzinfo=utc)
    date_now = datetime.now()
    date_now = date_now.replace(tzinfo=utc)
    if booking_date < date_now:
        booking_deatils[brn]["status"] = "Expired"
    else:
        booking_deatils[brn]["status"] = "Active"
    make_ticket_qr_code(booking_deatils)
    return render(request, 'movies/generated_ticket.html', {'boking_history': booking_deatils})